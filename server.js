const express = require("express");
const mysql = require("mysql");
const bodyParser = require('body-parser');
const app = express();
const bcrypt = require('bcrypt-nodejs');
const cookieParser = require('cookie-parser');
const session = require('express-session');

app.use(bodyParser.json());
var urlencodeparser = bodyParser.urlencoded({ extended: true });
app.use(urlencodeparser);
app.use(cookieParser());
app.use(session({secret: 'tout est crypté'}));
app.set('view engine', 'ejs');

app.use(express.static("public"));

/*Connection BDD*/
const db = mysql.createConnection({
    host: 'localhost' ,
    user: 'root',
    password: 'kenya2010',
    database: 'greenchallenge'
});

db.connect(function (err) {
    if (err) throw err;
    console.log("mySQL est connecté");
});
/*fin de la connection à la BDD*/

/*Début des routes */
app.get('/', function(req, res){
    res.render('accueil');
})
app.get('/connecter', function(req, res){
    res.render('connecter');
})
app.get('/visiteur', function(req, res){
    res.render('visiteur');
})
app.get('/menu', function(req, res){
    res.render('menu', {
        pseudo: sess.pseudo
    });
})
app.get('/profil', function(req, res) {
    res.render('profil', {
      pseudo: sess.pseudo  
    });
})
app.get('/ajouterdefi', function(req, res){
    res.render('ajouterdefi');
})
app.get('/listedefi', function(req, res){
    res.render('listedefi', {
        nom: sess.nom,
        photo: sess.photo
    });
});
app.get('/defi', function(req, res){
    res.render('defi');
})
/*Fin des routes */
let sess;
/* Je vérifie si l'adresse mail est déjà prise*/
app.post('/formajout', urlencodeparser, function (req, res) {
    var select = "SELECT pseudo, email, motdepasse FROM users WHERE email = (?);";
    var email = [req.body.email];
    select = mysql.format(select, email);
    db.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Cette adresse mail est déjà utilisée !")
        }
    });
    //J'insére le nouveau compte dans la BDD
    var sql = "INSERT INTO users (pseudo, email, motdepasse) VALUES (?, ?, ?);"
    var values = [req.body.pseudo, req.body.email, bcrypt.hashSync(req.body.motdepasse, null, null)];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.pseudo = req.body.pseudo;
    console.log(sess.pseudo);
    db.query(sql, function (error, results, fields){
        if (!error) {
            console.log("Utilisateur créer")
            res.redirect('/menu');
        } else {
            console.log(error)
            res.send(error);
        }
    })
}); 
app.post("/formconnect", urlencodeparser, function (req, res) {
    var sql = "SELECT pseudo, email, motdepasse, idusers FROM users WHERE pseudo = (?);";
    var values = [req.body.pseudo];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.pseudo = req.body.pseudo;
    console.log(sess.pseudo);
    db.query(sql, function (error, results, fields) {
        if(!error) {
            var msg = "Email ou pseudo invalide";
            if (results.length !=1) {
                console.log(results);
                res.send(msg);
            } else {
                if (bcrypt.compareSync(req.body.motdepasse, results[0].motdepasse)) {
                    logged_user = {
                        pseudo: results[0].pseudo,
                        email: results[0].email,
                        idusers: results[0].idusers
                    };
                    console.log(logged_user);
                    msg = "Bienvenue sur notre application";
                    console.log("utilisateur connecté");
                    res.redirect("/menu");
                } else {
                    res.send(msg);
                }
            }
        } else {
            res.send(error);
        }
    })
})
app.post("/createdefi", urlencodeparser, function (req, res) {
    var select = "SELECT nom, date, heure FROM challenges WHERE nom =(?);";
    var nom = [req.body.nom];
    select = mysql.format(select, nom);
    db.query(select, function (error, results, fields) {
        if (!error && results.length > 0) {
            res.send("Veuillez entrez un nom d'événement différent !")
        }
    });
    var sql = "INSERT INTO challenges (nom, photo, date, heure, adresse, description, users_idusers) VALUES (?,?,?,?,?,?,?);";
    var values = [req.body.nom, req.body.photo, req.body.date, req.body.heure, req.body.adresse, req.body.description, logged_user.idusers];
    sql = mysql.format(sql, values);
    sess = req.session;
    sess.nom = req.body.nom;
    sess.photo = req.body.photo;
    console.log(sess.nom);
    console.log(sess.photo);
    console.log(logged_user);
    db.query(sql, function (error, results, fields) {
        if (!error) {
            console.log("Le challenge à bien été ajouté")
            res.redirect("/listedefi");
        } else {
            console.log(error)
            res.send(error);
        }
    })
});
app.listen(3020, function () {
    console.log('le serveur écoute sur le port 3020');
});
