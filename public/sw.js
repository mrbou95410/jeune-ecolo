var cacheName = 'greenchallenge';
var filesToCache = [
  '/',
  'img/logo/logo128.png',
  'img/logo/logo144.png',
  'img/logo/logo158.png',
  'img/logo/logo192.png',
  'img/logo/logo512.png',
  'img/logo/logo.png',
  'img/avatar.png',
  'img/ecolo.jpg'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('fetch', event => {
  event.respondWith(
    caches.match(event.request, { ignoreSearch: true }).then(response => {
      return response || fetch(event.request);
    })
  );
});